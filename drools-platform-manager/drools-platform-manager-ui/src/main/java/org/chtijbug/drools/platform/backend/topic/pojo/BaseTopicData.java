package org.chtijbug.drools.platform.backend.topic.pojo;

/**
 * Created by IntelliJ IDEA.
 * Date: 12/05/14
 * Time: 09:59
 * To change this template use File | Settings | File Templates.
 */
public class BaseTopicData {
    private Long ruleBaseID;

    public BaseTopicData() {
    }

    public Long getRuleBaseID() {
        return ruleBaseID;
    }

    public void setRuleBaseID(Long ruleBaseID) {
        this.ruleBaseID = ruleBaseID;
    }
}
