/*
 * Copyright 2014 Pymma Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.chtijbug.drools.platform.persistence.pojo;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "ruleflowgroup", indexes = {@Index(columnList = "ruleflowGroup")})
@Cacheable(value = true)
public class RuleflowGroup {

    @Id
    @SequenceGenerator(name = "ruleflowgroup_id_seq", sequenceName = "ruleflowgroup_platform_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ruleflowgroup_id_seq")
    private Long id;

    @Column(nullable = false)
    private String ruleflowGroup;

    @ManyToOne
    @JoinColumn(name = "process_execution_id_fk", referencedColumnName = "id")
    private ProcessExecution processExecution;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ruleflowGroup")
    private List<RuleExecution> ruleExecutionList = new ArrayList<RuleExecution>();

    @Column(nullable = false)
    private Date startDate;

    private Date endDate;

    private Long startEventID;

    private Long stopEventID;

    @Enumerated(EnumType.STRING)
    private RuleflowGroupStatus ruleflowGroupStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRuleflowGroup() {
        return ruleflowGroup;
    }

    public void setRuleflowGroup(String ruleflowGroup) {
        this.ruleflowGroup = ruleflowGroup;
    }

    public ProcessExecution getProcessExecution() {
        return processExecution;
    }

    public void setProcessExecution(ProcessExecution processExecution) {
        this.processExecution = processExecution;
    }

    public List<RuleExecution> getRuleExecutionList() {
        return ruleExecutionList;
    }

    public void setRuleExecutionList(List<RuleExecution> ruleExecutionList) {
        this.ruleExecutionList = ruleExecutionList;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public RuleflowGroupStatus getRuleflowGroupStatus() {
        return ruleflowGroupStatus;
    }

    public void setRuleflowGroupStatus(RuleflowGroupStatus ruleflowGroupStatus) {
        this.ruleflowGroupStatus = ruleflowGroupStatus;
    }

    public Long getStartEventID() {
        return startEventID;
    }

    public void setStartEventID(Long startEventID) {
        this.startEventID = startEventID;
    }

    public Long getStopEventID() {
        return stopEventID;
    }

    public void setStopEventID(Long stopEventID) {
        this.stopEventID = stopEventID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RuleflowGroup)) return false;

        RuleflowGroup that = (RuleflowGroup) o;

        if (!processExecution.equals(that.processExecution)) return false;
        if (!ruleflowGroup.equals(that.ruleflowGroup)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ruleflowGroup.hashCode();
        result = 31 * result + processExecution.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RuleflowGroupRuntime{");
        sb.append("id=").append(id);
        sb.append(", ruleflowGroup='").append(ruleflowGroup).append('\'');
        sb.append(", startDate=").append(startDate);
        sb.append(", endDate=").append(endDate);
        sb.append(", ruleflowGroupRuntimeStatus=").append(ruleflowGroupStatus);
        sb.append('}');
        return sb.toString();
    }
}
